module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    rootDir: '.',
    roots: ['<rootDir>/src'],
    testMatch: ['**/?(*.)+(spec|test).[tj]s?(x)'],
    clearMocks: true,
    collectCoverage: true,
    collectCoverageFrom: ['src/**/*.{ts,tsx}'],
    coveragePathIgnorePatterns: [
        '/dist/',
        '/node_modules/',
        '/src/__tests__/',
        '/src/index.ts',
    ],
    moduleFileExtensions: ['ts', 'js'],
}
