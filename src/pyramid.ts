const character = 'x';

const validatePyramidRows = (rows: any): boolean => {
    if (typeof rows !== 'number') {
        throw new Error('"row" parameter must be a number.');
    }
    if (Math.abs(rows) === 0) {
        throw new Error('Zero is not allowed.');
    }
    return true
}

export function printPyramid(rows: any): string  {
    validatePyramidRows(rows)
    // Don't set row again, this is being passed by reference
    // Create new variable instead here.
    const outputRows = Math.abs(rows)
    if (outputRows === 1) {
        return character;
    }
    const pyramid = []
    for (let i = 1; i <= outputRows; i++) {
        pyramid.push(character.repeat(i))
    }
    return pyramid.join('\n');
}

export function printPyramidNoLoop(rows: any): string  {
    validatePyramidRows(rows)
    // Don't set row again, this is being passed by reference
    // Create new variable instead here.
    const outputRows = Math.abs(rows)
    if (outputRows === 1) {
        return character;
    }
    return Array.from(Array(outputRows), (_, index) => character.repeat(index + 1)).join('\n')
}
