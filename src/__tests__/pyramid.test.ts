import { printPyramid, printPyramidNoLoop } from '../pyramid'

describe('printPyramid', (): void => {
    it.each([
        [5],
        [7],
        [-2],
        [1],
    ])('returns correct pattern when %p rows is passed', (rows): void => {
        expect(printPyramid(rows)).toMatchSnapshot()
    })

    it.each([
        [0],
        ['foobar'],
    ])('returns error message when %s is passed', (rows): void => {
        expect(() => printPyramid(rows)).toThrowErrorMatchingSnapshot()
    })
})

describe('printPyramidNoLoop', (): void => {
    it.each([
        [5],
        [7],
        [-2],
        [1],
    ])('returns correct pattern when %p rows is passed', (rows): void => {
        expect(printPyramidNoLoop(rows)).toMatchSnapshot()
    })

    it.each([
        [0],
        ['foobar'],
    ])('returns error message when %s is passed', (rows): void => {
        expect(() => printPyramidNoLoop(rows)).toThrowErrorMatchingSnapshot()
    })
})
