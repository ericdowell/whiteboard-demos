import { printPyramid, printPyramidNoLoop } from './pyramid'

console.log('Output 1:')
console.log(printPyramid(1), '\n')

console.log('Output 5:')
console.log(printPyramid(5), '\n')

console.time('printPyramid')
console.log('Output 7:')
console.log(printPyramid(7))
console.timeEnd('printPyramid')

console.time('printPyramidNoLoop')
console.log('\nOutput 7:')
console.log(printPyramidNoLoop(7))
console.timeEnd('printPyramidNoLoop')
