# Q3 Whiteboard Demos
Clone the repo, run `npm i` within project root and run `npm start` to see output of pyramid.

## Tests
```
npm t
```

# 2. Whiteboard Challenge

Write a function that takes in an integer, and prints a pyramid of `x` characters based off of that number.

Your code can be in whatever language you'd like, including pseudo-code.

## Example 1

```javascript
printPyramid(5);
```

Output:

```shell
x
xx
xxx
xxxx
xxxxx
```

## Example 2

```javascript
printPyramid(7);
```

Output:

```shell
x
xx
xxx
xxxx
xxxxx
xxxxxx
xxxxxxx
```
